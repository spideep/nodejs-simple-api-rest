'use strict';

// List.
exports.list = (req, res) => {
  let courses = require('./../models/Courses');
  let courseTypes = require('./../models/CourseTypes');
  let courses_m = [];

  courses.forEach((item) => {
    let course = item;
    let thiscat = courseTypes.filter(item => {
      return item.id == course.typeId
    });

    item.catname = thiscat[0].name;
    courses_m.push(item);
  })
  res.json(courses_m);
}

exports.listfiltered = (req, res) => {
  let courses = require('./../models/Courses');
  let courses_m = [];

  const courseId = parseInt(req.params.courseId);
  const cdate = decodeURIComponent(req.params.date);

  courses.forEach(() => {
    courses_m = courses.filter(course => {
      if (courseId) {
        return course.typeId === courseId && course.releaseDate === cdate
      }
      else {
        return course.releaseDate === cdate
      }
    });
  })
  res.json(courses_m);
}

exports.types = (req, res) => {
  let courseTypes = require('./../models/CourseTypes');
  res.json(courseTypes);
}
