module.exports = [
  {
    id: 1,
    name: "Campaign A"
  },
  {
    id: 2,
    name: "Campaign B"
  },
  {
    id: 3,
    name: "Campaign C"
  },
  {
    id: 4,
    name: "Campaign D"
  },
  {
    id: 5,
    name: "Campaign E"
  },
  {
    id: 6,
    name: "Campaign F"
  },
  {
    id: 7,
    name: "Campaign G"
  },
  {
    id: 8,
    name: "Campaign H"
  }
];