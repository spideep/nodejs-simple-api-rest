'use strict';
module.exports = function (app) {
  let course = require('../controllers/courseController');

  // course Routes.
  app.route('/courses')
    .get(course.list)

  app.route('/coursetypes')
    .get(course.types)

  app.route('/courses/:courseId/:date')
    .get(course.listfiltered)
  //   .post(course.update)
  //   .delete(course.delete);
}